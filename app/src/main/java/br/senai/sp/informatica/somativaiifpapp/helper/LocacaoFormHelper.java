package br.senai.sp.informatica.somativaiifpapp.helper;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.cadastro.LocacaoCadastro;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoFormHelper {

    private TextView dataIn;
    private TextView dataFi;
    private EditText passageiros;
    private EditText origem;
    private EditText destino;
    private CheckBox motorista;
    private Button alocar;
    private Locacao locacao;

    public Button getAlocar() {
        return alocar;
    }

    public LocacaoFormHelper(LocacaoCadastro cad){
        dataIn = cad.findViewById(R.id.dataInicioEt);
        dataFi = cad.findViewById(R.id.dataFinalEt);
        passageiros = cad.findViewById(R.id.qtdPassageiros);
        origem = cad.findViewById(R.id.origem);
        destino = cad.findViewById(R.id.destino);
        motorista = cad.findViewById(R.id.motoristaCb);
        alocar = cad.findViewById(R.id.btnAlocar);
        locacao = new Locacao();
    }

    public Locacao pegarLocacao(){
        locacao = new Locacao();
        locacao.setQtnPassageiros(Byte.parseByte(passageiros.getText().toString()));
        locacao.setOrigem(origem.getText().toString());
        locacao.setDestino(destino.getText().toString());
        return locacao;
    }
}
