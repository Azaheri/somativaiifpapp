package br.senai.sp.informatica.somativaiifpapp.model;

import java.io.Serializable;
import java.util.Date;

public class Locacao implements Serializable{
    private Long id;
    private Date dataEntrada;
    private Date dataDevolucao;
    private Boolean motorista;
    private Byte qtnPassageiros;
    private String origem;
    private String destino;
    private Long duracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Boolean getMotorista() {
        return motorista;
    }

    public void setMotorista(Boolean motorista) {
        this.motorista = motorista;
    }

    public Byte getQtnPassageiros() {
        return qtnPassageiros;
    }

    public void setQtnPassageiros(Byte qtnPassageiros) {
        this.qtnPassageiros = qtnPassageiros;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Long getDuracao() {
        return duracao;
    }

    public void setDuracao(Long duracao) {
        this.duracao = duracao;
    }

    @Override
    public String toString() {
        return "\nLocacao{" +
                "id=" + id +
                ", dataEntrada=" + dataEntrada +
                ", dataDevolucao=" + dataDevolucao +
                ", motorista=" + motorista +
                ", qtnPassageiros=" + qtnPassageiros +
                ", origem='" + origem + '\'' +
                ", destino='" + destino + '\'' +
                ", duracao=" + duracao +
                '}';
    }
}
