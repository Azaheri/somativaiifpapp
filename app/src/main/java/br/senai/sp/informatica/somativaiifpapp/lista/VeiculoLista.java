package br.senai.sp.informatica.somativaiifpapp.lista;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

import br.senai.sp.informatica.somativaiifpapp.DAO.VeiculoDAO;
import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.adapter.VeiculoAdapter;
import br.senai.sp.informatica.somativaiifpapp.cadastro.VeiculoCadastro;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoLista extends AppCompatActivity {

    private RecyclerView listaVeiculos;
    private VeiculoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculo_lista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listaVeiculos = findViewById(R.id.rvVeiculoId);
        dao = new VeiculoDAO(this);
        List<Veiculo> veiculos = dao.procurarVeiculos();
        carregarLista(veiculos);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VeiculoLista.this, VeiculoCadastro.class);
                startActivity(intent);
            }
        });
    }

    private void carregarLista(List<Veiculo> veiculos) {
        VeiculoAdapter adapter = new VeiculoAdapter(this, veiculos);
        listaVeiculos.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        listaVeiculos.setLayoutManager(layoutManager);
    }


    @Override
    protected void onResume() {
        super.onResume();
        dao = new VeiculoDAO(this);
        carregarLista(dao.procurarVeiculos());
    }
    
}
