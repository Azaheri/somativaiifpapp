package br.senai.sp.informatica.somativaiifpapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.zip.Inflater;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.holder.VeiculoViewHolder;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoAdapter extends RecyclerView.Adapter{

    private final List<Veiculo> veiculos;
    private final Context context;

    public VeiculoAdapter(Context context, List<Veiculo> veiculos) {
        this.context = context;
        this.veiculos = veiculos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_list, parent, false);
        VeiculoViewHolder holder = new VeiculoViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VeiculoViewHolder viewHolder = (VeiculoViewHolder) holder;
        Veiculo veiculo = veiculos.get(position);
        viewHolder.preencher(veiculo);
    }

    @Override
    public int getItemCount() {
        return veiculos.size();
    }
}
