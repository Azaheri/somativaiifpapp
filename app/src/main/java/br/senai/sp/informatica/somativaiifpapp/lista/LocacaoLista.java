package br.senai.sp.informatica.somativaiifpapp.lista;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.List;

import br.senai.sp.informatica.somativaiifpapp.DAO.LocacaoDAO;
import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.adapter.LocacaoAdapter;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoLista extends AppCompatActivity {

    private RecyclerView listaLocacoes;
    private LocacaoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locacao_lista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listaLocacoes = findViewById(R.id.rvLocacaoId);
        dao = new LocacaoDAO(this);
        List<Locacao> locacoes = dao.procurarLocacoes();
        carregarLista(locacoes);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LocacaoLista.this, VeiculoLista.class);
                startActivity(intent);
            }
        });

        Log.e("sdaji", locacoes.toString());
    }

    private void carregarLista(List<Locacao> locacoes) {
        LocacaoAdapter adapter = new LocacaoAdapter(this, locacoes);
        listaLocacoes.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        listaLocacoes.setLayoutManager(layoutManager);
    }


    @Override
    protected void onResume() {
        super.onResume();
        dao = new LocacaoDAO(this);
        carregarLista(dao.procurarLocacoes());
    }

}
