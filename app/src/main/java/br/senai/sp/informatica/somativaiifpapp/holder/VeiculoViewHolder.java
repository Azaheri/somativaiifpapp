package br.senai.sp.informatica.somativaiifpapp.holder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.adapter.VeiculoAdapter;
import br.senai.sp.informatica.somativaiifpapp.cadastro.VeiculoCadastro;
import br.senai.sp.informatica.somativaiifpapp.helper.VeiculoFormHelper;
import br.senai.sp.informatica.somativaiifpapp.lista.VeiculoListaDetalhada;
import br.senai.sp.informatica.somativaiifpapp.model.OnibusVan;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoViewHolder extends RecyclerView.ViewHolder{

    private final VeiculoAdapter veiculoAdapter;
    private TextView modelo;
    private TextView dataFabricacao;
    private ImageView fotoVeiculo;
    private TextView tipo;
    private Long veiculoId;
    private Byte assentos;
    private VeiculoFormHelper helper;

    public VeiculoViewHolder(View itemView, VeiculoAdapter veiculoAdapter) {
        super(itemView);
        this.veiculoAdapter = veiculoAdapter;

        fotoVeiculo = itemView.findViewById(R.id.item_imagem);
        modelo = itemView.findViewById(R.id.item_veiculo);
        dataFabricacao = itemView.findViewById(R.id.item_data);
        tipo = itemView.findViewById(R.id.item_tipo);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Activity context = (Activity)view.getContext();
                final Intent intent = new Intent(context, VeiculoListaDetalhada.class);
                fotoVeiculo.setDrawingCacheEnabled(true);
                Bitmap bm = fotoVeiculo.getDrawingCache();
                intent.putExtra("foto", bm);
                intent.putExtra("modelo", modelo.getText());
                intent.putExtra("dataFabricacao", dataFabricacao.getText());
                intent.putExtra("tipo", tipo.getText());
                intent.putExtra("assentos", assentos.byteValue());
                context.startActivityForResult(intent, 1);

            }
        });
    }

    public void preencher(Veiculo veiculo){
        veiculoId = veiculo.getId();
        assentos = veiculo.getQtnAssentos();
        modelo.setText(veiculo.getModelo());
        tipo.setText(veiculo.getTipoVeiculo().toString());
        dataFabricacao.setText(veiculo.getAnoFabricacao());
        String caminhoFoto = veiculo.getCaminhoFoto();
        if (caminhoFoto != null) {
            Bitmap fotoRetornada = BitmapFactory.decodeFile(veiculo.getCaminhoFoto());
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(fotoRetornada, 400, 250, true);
            fotoVeiculo.setImageBitmap(bitmapReduzido);
            fotoVeiculo.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }
}
