package br.senai.sp.informatica.somativaiifpapp.lista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoListaDetalhada extends AppCompatActivity {

    private TextView dataEntrada;
    private TextView dataDevolucao;
    private TextView motorista;
    private TextView qtnPassageiros;
    private TextView origem;
    private TextView destino;
    private TextView duracao;
    private TextView preco;
    private Locacao locacao;
    private Integer precoMotorista = 0;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locacao_lista_detalhada);

        dataEntrada = findViewById(R.id.locEntrada);
        dataDevolucao = findViewById(R.id.locDevolucao);
        motorista = findViewById(R.id.locMotorista);
        qtnPassageiros = findViewById(R.id.locPassageiros);
        origem = findViewById(R.id.locOrigem);
        destino = findViewById(R.id.locDestino);
        duracao = findViewById(R.id.locDuracao);
        preco = findViewById(R.id.locPreco);

        final Bundle extras = getIntent().getExtras();

        locacao = (Locacao) extras.getSerializable("Locacao");

        dataEntrada.setText("Data de Embarque : " + sdf.format(locacao.getDataEntrada()));
        dataDevolucao.setText("Data de Retorno : " + sdf.format(locacao.getDataDevolucao()));
        motorista.setText("Motorista disponível : " + locacao.getMotorista());
        qtnPassageiros.setText("Qtd. Passageiros : "+ locacao.getQtnPassageiros());
        origem.setText("Origem : " + locacao.getOrigem());
        destino.setText("Destino : " + locacao.getDestino());
        duracao.setText("Duração : " + locacao.getDuracao());
        if (locacao.getMotorista()){
            precoMotorista = 200;
        }
        Long preco2 = precoMotorista + (25 * locacao.getDuracao());
        preco.setText("Preço : R$" + preco2);
    }

    public void voltarMenu(View view) {
        finish();
    }
}
