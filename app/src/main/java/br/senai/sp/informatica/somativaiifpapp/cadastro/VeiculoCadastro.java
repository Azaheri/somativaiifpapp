package br.senai.sp.informatica.somativaiifpapp.cadastro;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import br.senai.sp.informatica.somativaiifpapp.DAO.VeiculoDAO;
import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.helper.VeiculoFormHelper;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoCadastro extends AppCompatActivity {

    public static final int CODIGO_GALERIA = 1;
    public static final int PERMISSAO_REQUEST = 1;
    private Button addVeiculo;
    private VeiculoFormHelper helper;
    private ImageView imagemCarregada;
    private VeiculoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculo_cadastro);

        helper = new VeiculoFormHelper(this);
        dao = new VeiculoDAO(getApplicationContext());
        imagemCarregada = helper.getImagem();
        addVeiculo = helper.getBotaoAdicionar();

        imagemCarregada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, CODIGO_GALERIA);
            }
        });

        addVeiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Veiculo veiculo = helper.pegarVeiculo();
                dao = new VeiculoDAO(VeiculoCadastro.this);
                dao.inserir(veiculo);
                finish();
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSAO_REQUEST); // Control Alt C -> converte alguma coisa em constante
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODIGO_GALERIA){
            Uri uri = data.getData();
            String[] arquivoCaminho = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(uri, arquivoCaminho, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(arquivoCaminho[0]);
            String caminhoFoto = c.getString(columnIndex);
            c.close();
            Bitmap fotoRetorno = (BitmapFactory.decodeFile(caminhoFoto));
            imagemCarregada.setImageBitmap(fotoRetorno);
            imagemCarregada.setTag(caminhoFoto);
        }
    }

}
