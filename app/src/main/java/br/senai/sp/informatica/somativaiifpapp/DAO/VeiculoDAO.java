package br.senai.sp.informatica.somativaiifpapp.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.somativaiifpapp.model.OnibusVan;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoDAO extends SQLiteOpenHelper{

    public VeiculoDAO(Context context) {
        super(context, "FPApp", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Veiculo(id INTEGER PRIMARY KEY, modelo TEXT NOT NULL, caminhoFoto TEXT, anoFabricacao TEXT NOT NULL, qtnAssentos TINYINT NOT NULL, tipoVeiculo TEXT NOT NULL)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Veiculo";
        db.execSQL(sql);
    }

    @NonNull
    private ContentValues pegarInformacoes(Veiculo veiculo){
        ContentValues informacao = new ContentValues();
        informacao.put("modelo", veiculo.getModelo());
        informacao.put("caminhoFoto", veiculo.getCaminhoFoto());
        informacao.put("anoFabricacao", veiculo.getAnoFabricacao());
        informacao.put("qtnAssentos", veiculo.getQtnAssentos());
        informacao.put("tipoVeiculo", veiculo.getTipoVeiculo().name());
        return informacao;
    }

    public void inserir(Veiculo veiculo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues informacao = pegarInformacoes(veiculo);
        db.insert("Veiculo", null, informacao);
    }

    public List<Veiculo> procurarVeiculos(){
        String sql = "SELECT * FROM Veiculo";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Veiculo> veiculos = new ArrayList<>();

        while(c.moveToNext()){
            Veiculo veiculo = new Veiculo();
            veiculo.setId(c.getLong(c.getColumnIndex("id")));
            veiculo.setModelo(c.getString(c.getColumnIndex("modelo")));
            veiculo.setCaminhoFoto(c.getString(c.getColumnIndex("caminhoFoto")));
            veiculo.setAnoFabricacao(c.getString(c.getColumnIndex("anoFabricacao")));
            veiculo.setQtnAssentos((byte) c.getShort(c.getColumnIndex("qtnAssentos")));
            veiculo.setTipoVeiculo(OnibusVan.valueOf(OnibusVan.class, c.getString(c.getColumnIndex("tipoVeiculo"))));
            veiculos.add(veiculo);
        }
        return veiculos;
    }

}
