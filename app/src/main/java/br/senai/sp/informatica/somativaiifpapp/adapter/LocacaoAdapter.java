package br.senai.sp.informatica.somativaiifpapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.holder.LocacaoViewHolder;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoAdapter extends RecyclerView.Adapter{

    private final List<Locacao> locacoes;
    private final Context context;

    public LocacaoAdapter(Context context, List<Locacao> locacao) {
        this.context = context;
        this.locacoes = locacao;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_list_locacao, parent, false);
        LocacaoViewHolder holder = new LocacaoViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LocacaoViewHolder viewHolder = (LocacaoViewHolder) holder;
        Locacao locacao = locacoes.get(position);
        viewHolder.preencher(locacao);
    }

    @Override
    public int getItemCount() {
        return locacoes.size();
    }
}
