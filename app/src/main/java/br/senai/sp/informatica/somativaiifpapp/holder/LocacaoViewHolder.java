package br.senai.sp.informatica.somativaiifpapp.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.adapter.LocacaoAdapter;
import br.senai.sp.informatica.somativaiifpapp.cadastro.LocacaoCadastro;
import br.senai.sp.informatica.somativaiifpapp.lista.LocacaoLista;
import br.senai.sp.informatica.somativaiifpapp.lista.LocacaoListaDetalhada;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoViewHolder extends RecyclerView.ViewHolder{

    private LocacaoAdapter locacaoAdapter;
    private Long locacaoId;
    private TextView partida;
    private TextView retorno;
    private TextView origem;
    private TextView destino;
    private Locacao locacao;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");


    public LocacaoViewHolder(final View view, LocacaoAdapter locacaoAdapter) {
        super(view);
        this.locacaoAdapter = locacaoAdapter;

        partida = view.findViewById(R.id.item_dataEntrada);
        retorno = view.findViewById(R.id.item_dataSaida);
        origem = view.findViewById(R.id.item_origem);
        destino = view.findViewById(R.id.item_destino);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity context = (Activity) view.getContext();
                final Intent intent = new Intent(context, LocacaoListaDetalhada.class);

                intent.putExtra("Locacao", locacao);
                context.startActivityForResult(intent, 1);
            }
        });
    }

    public void preencher(Locacao locacao) {
        this.locacao = locacao;
        locacaoId = locacao.getId();
        partida.setText(sdf.format(locacao.getDataEntrada()));
        retorno.setText(sdf.format(locacao.getDataDevolucao()));
        origem.setText(locacao.getOrigem());
        destino.setText(locacao.getDestino());
    }
}
