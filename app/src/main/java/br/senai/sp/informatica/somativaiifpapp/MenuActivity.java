package br.senai.sp.informatica.somativaiifpapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Date;

import br.senai.sp.informatica.somativaiifpapp.DAO.LocacaoDAO;
import br.senai.sp.informatica.somativaiifpapp.cadastro.LocacaoCadastro;
import br.senai.sp.informatica.somativaiifpapp.cadastro.VeiculoCadastro;
import br.senai.sp.informatica.somativaiifpapp.lista.LocacaoLista;
import br.senai.sp.informatica.somativaiifpapp.lista.VeiculoLista;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    private Button cadVeic;
    private Button cadLoc;
    private Button listVeic;
    private Button listLoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        cadVeic = findViewById(R.id.btnCadVeic);
        cadLoc = findViewById(R.id.btnCadLoc);
        listVeic = findViewById(R.id.btnListVeic);
        listLoc = findViewById(R.id.btnListLoc);

        cadVeic.setOnClickListener(this);
        cadLoc.setOnClickListener(this);
        listVeic.setOnClickListener(this);
        listLoc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btnCadVeic:
                Intent cadVeic = new Intent(this, VeiculoCadastro.class);
                startActivity(cadVeic);
                break;
            case R.id.btnCadLoc:
                Intent cadLoc = new Intent(this, VeiculoLista.class);
                startActivity(cadLoc);
                break;
            case R.id.btnListVeic:
                Intent listVeic = new Intent(this, VeiculoLista.class);
                startActivity(listVeic);
                break;
            case R.id.btnListLoc:
                Intent listLoc = new Intent(this, LocacaoLista.class);
                startActivity(listLoc);
                break;
        }
    }
}
