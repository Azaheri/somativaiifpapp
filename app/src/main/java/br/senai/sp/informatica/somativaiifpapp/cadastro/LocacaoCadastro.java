package br.senai.sp.informatica.somativaiifpapp.cadastro;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.senai.sp.informatica.somativaiifpapp.DAO.LocacaoDAO;
import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.helper.LocacaoFormHelper;
import br.senai.sp.informatica.somativaiifpapp.lista.LocacaoLista;
import br.senai.sp.informatica.somativaiifpapp.lista.LocacaoListaDetalhada;
import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoCadastro extends AppCompatActivity {

    private TextView dataIn, dataFi;
    private Button alocar;
    private LocacaoFormHelper helper;
    private DatePickerDialog.OnDateSetListener dateListenerIn, dateListenerOut;
    private LocalDate entradaTratada, saidaTratada;
    private LocacaoDAO dao;

    private Date primeiraData = null;
    private Date segundaData = null;

    private CheckBox motorista;
    private Boolean moto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locacao_cadastro);

        dataIn = findViewById(R.id.dataInicioEt);
        dataFi = findViewById(R.id.dataFinalEt);
        alocar = findViewById(R.id.btnAlocar);
        motorista = findViewById(R.id.motoristaCb);

        helper = new LocacaoFormHelper(this);

        dataIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendario = Calendar.getInstance();
                int year = calendario.get(Calendar.YEAR);
                int month = calendario.get(Calendar.MONTH);
                int day = calendario.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(LocacaoCadastro.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateListenerIn, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        }); // Fim do onclick

        dataFi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendario = Calendar.getInstance();
                int year = calendario.get(Calendar.YEAR);
                int month = calendario.get(Calendar.MONTH);
                int day = calendario.get(Calendar.DAY_OF_MONTH);
//                segundaData = calendario.getTime();
                DatePickerDialog dialog = new DatePickerDialog(LocacaoCadastro.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateListenerOut, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        }); // Fim do onclick


        dateListenerIn = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new GregorianCalendar(year, month, dayOfMonth).getTime();
                month +=1;
                String dataEntradaRetornada = dayOfMonth+"/"+month+"/"+year;
                entradaTratada = LocalDate.of(year, month, dayOfMonth);
                primeiraData = date;
                dataIn.setText(dataEntradaRetornada);
            }
        }; // Fim do onclick

        dateListenerOut = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date date = new GregorianCalendar(year, month, dayOfMonth).getTime();
                month +=1;
                String dataSaidaRetornada = dayOfMonth+"/"+month+"/"+year;
                saidaTratada = LocalDate.of(year, month, dayOfMonth);
                segundaData = date;
                dataFi.setText(dataSaidaRetornada);
            }
        }; // Fim do onclick

        alocar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (entradaTratada == null || saidaTratada == null){
                    Toast.makeText(LocacaoCadastro.this, "Insira as datas corretamente", Toast.LENGTH_SHORT).show();
                }else {
                    Long diferencaData = entradaTratada.until(saidaTratada, ChronoUnit.DAYS);
                    final Locacao locacao = helper.pegarLocacao();
                    dao = new LocacaoDAO(LocacaoCadastro.this);
                    locacao.setDataEntrada(primeiraData);
                    locacao.setDataDevolucao(segundaData);
                    locacao.setDuracao(diferencaData);
                    locacao.setMotorista(motorista.isChecked());

                    dao.inserir(locacao);
                    Intent intent = new Intent(LocacaoCadastro.this, LocacaoListaDetalhada.class);
                    intent.putExtra("Locacao", locacao);
                    startActivity(intent);
                    finish();
                }

            }
        });




    }
}
