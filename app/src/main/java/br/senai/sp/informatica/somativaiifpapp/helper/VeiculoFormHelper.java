package br.senai.sp.informatica.somativaiifpapp.helper;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.cadastro.VeiculoCadastro;
import br.senai.sp.informatica.somativaiifpapp.model.OnibusVan;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoFormHelper {

    private ImageView imagem;
    private EditText modelo;
    private EditText dataFabricacao;
    private EditText qtdAssentos;
    private RadioGroup tipoVeiculo;
    private Button botaoAdicionar;
    private Veiculo veiculo;

    public ImageView getImagem() {
        return imagem;
    }

    public Button getBotaoAdicionar() {
        return botaoAdicionar;
    }

    public VeiculoFormHelper(VeiculoCadastro cad){
        imagem = cad.findViewById(R.id.editImagem);
        modelo = cad.findViewById(R.id.editModelo);
        dataFabricacao = cad.findViewById(R.id.editDataFabricacao);
        qtdAssentos = cad.findViewById(R.id.editQtdAssentos);
        tipoVeiculo = cad.findViewById(R.id.tipoVeiculo);
        botaoAdicionar = cad.findViewById(R.id.btnRegister);
        veiculo = new Veiculo();
    }

    public Veiculo pegarVeiculo(){
        veiculo.setCaminhoFoto(imagem.getTag().toString());
        veiculo.setModelo(modelo.getText().toString());
        veiculo.setAnoFabricacao(dataFabricacao.getText().toString());
        veiculo.setQtnAssentos(Byte.parseByte(qtdAssentos.getText().toString()));
        switch (tipoVeiculo.getCheckedRadioButtonId()){
            case R.id.radioVan:
                veiculo.setTipoVeiculo(OnibusVan.VAN.valueOf(OnibusVan.VAN.toString()));
                break;
            case R.id.radioOnibus:
                veiculo.setTipoVeiculo(OnibusVan.ONIBUS.valueOf(OnibusVan.ONIBUS.toString()));
                break;
        }
        return veiculo;
    }
}
