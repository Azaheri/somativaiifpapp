package br.senai.sp.informatica.somativaiifpapp.model;

import java.io.Serializable;

public class Veiculo implements Serializable {
    private Long id;
    private String modelo;
    private String anoFabricacao;
    private Byte qtnAssentos;
    private OnibusVan tipoVeiculo;
    private String caminhoFoto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Byte getQtnAssentos() {
        return qtnAssentos;
    }

    public void setQtnAssentos(Byte qtnAssentos) {
        this.qtnAssentos = qtnAssentos;
    }

    public OnibusVan getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(OnibusVan tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String caminhoFoto) {
        this.caminhoFoto = caminhoFoto;
    }

    @Override
    public String toString() {
        return "\nVeiculo{" +
                "id=" + id +
                ", modelo='" + modelo + '\'' +
                ", anoFabricacao='" + anoFabricacao + '\'' +
                ", qtnAssentos=" + qtnAssentos +
                ", tipoVeiculo=" + tipoVeiculo +
                ", caminhoFoto='" + caminhoFoto + '\'' +
                '}';
    }
}
