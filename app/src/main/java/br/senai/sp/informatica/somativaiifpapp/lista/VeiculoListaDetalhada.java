package br.senai.sp.informatica.somativaiifpapp.lista;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.senai.sp.informatica.somativaiifpapp.R;
import br.senai.sp.informatica.somativaiifpapp.cadastro.LocacaoCadastro;
import br.senai.sp.informatica.somativaiifpapp.holder.VeiculoViewHolder;
import br.senai.sp.informatica.somativaiifpapp.model.Veiculo;

public class VeiculoListaDetalhada extends AppCompatActivity {

    private ImageView foto;
    private TextView modelo;
    private TextView fabricacao;
    private TextView assentos;
    private TextView tipo;
    private Button selecao;
    private Long veiculoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veiculo_lista_detalhada);

        foto = findViewById(R.id.fotoVeiculoId);
        modelo = findViewById(R.id.modeloVeiculoId);
        fabricacao = findViewById(R.id.fabricacaoVeiculoId);
        assentos = findViewById(R.id.assentosVeiculoId);
        tipo = findViewById(R.id.tipoVeiculoId);
        selecao = findViewById(R.id.btnSelecionar);


        final Bundle extras = getIntent().getExtras();
        Bitmap bitmap = extras.getParcelable("foto");
        foto.setImageBitmap(bitmap);
        modelo.setText(extras.getString("modelo"));
        fabricacao.setText(extras.getString("dataFabricacao"));
        tipo.setText((CharSequence) extras.get("tipo"));
        assentos.setText(String.valueOf(extras.getByte("assentos")));
        Log.e("assentos", String.valueOf(assentos));
        Log.e("id", String.valueOf(veiculoId));

        selecao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VeiculoListaDetalhada.this, LocacaoCadastro.class);
                startActivity(intent);
            }
        });
    }
}
