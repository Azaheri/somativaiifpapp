package br.senai.sp.informatica.somativaiifpapp.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.senai.sp.informatica.somativaiifpapp.model.Locacao;

public class LocacaoDAO extends SQLiteOpenHelper {
    public LocacaoDAO(Context context) {
        super(context, "FPAppLocacao", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Locacao(id INTEGER PRIMARY KEY, dataEntrada INTEGER  NOT NULL, dataDevolucao INTEGER NOT NULL," +
                " motorista TEXT NOT NULL, qtnPassageiros TINYINT NOT NULL, origem TEXT NOT NULL, destino TEXT NOT NULL, duracao INTEGER NOT NULL)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Locacao";
        db.execSQL(sql);
    }

    @NonNull
    private ContentValues pegarInformacoes(Locacao locacao){
        ContentValues informacao = new ContentValues();
        informacao.put("dataEntrada", locacao.getDataEntrada().getTime());
        informacao.put("dataDevolucao", locacao.getDataDevolucao().getTime());
        Log.e("dataEntrada", locacao.getDataEntrada().toString());
        Log.e("dataDevolução", locacao.getDataDevolucao().toString());
        informacao.put("motorista", locacao.getMotorista());
        informacao.put("qtnPassageiros", locacao.getQtnPassageiros());
        informacao.put("origem", locacao.getOrigem());
        informacao.put("destino", locacao.getDestino());
        informacao.put("duracao", locacao.getDuracao());
        return informacao;
    }

    public void inserir(Locacao locacao) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues informacao = pegarInformacoes(locacao);
        db.insert("Locacao", null, informacao);
    }

    public List<Locacao> procurarLocacoes(){
        String sql = "SELECT * FROM Locacao";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Locacao> locacoes = new ArrayList<>();

        while(c.moveToNext()){
            Locacao locacao = new Locacao();
            locacao.setId(c.getLong(c.getColumnIndex("id")));
            locacao.setDataEntrada(new Date(c.getLong(c.getColumnIndex("dataEntrada"))));
            locacao.setDataDevolucao(new Date(c.getLong(c.getColumnIndex("dataDevolucao"))));
            locacao.setMotorista(Boolean.valueOf(c.getString(c.getColumnIndex("motorista"))));
            locacao.setQtnPassageiros((byte) c.getShort(c.getColumnIndex("qtnPassageiros")));
            locacao.setOrigem(c.getString(c.getColumnIndex("origem")));
            locacao.setDestino(c.getString(c.getColumnIndex("destino")));
            locacao.setDuracao(Long.valueOf(c.getString(c.getColumnIndex("duracao"))));
            locacoes.add(locacao);
        }
        return locacoes;
    }
}
